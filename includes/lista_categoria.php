<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->
<div class="container m-t60">
    <div class="row bg_cat justify-content-md-center">

        <div class="col-11 p-0">
            <div class=" lista_categorias">
                <li id="slider_categorias" class="flexslider">
                    <ul class="slides">
                        <?php
                        $i = 0;

                        $result1 = $obj_site->select("tb_categorias_servicos", "and exibir_home = 'SIM'");
                        if (mysql_num_rows($result1) > 0) {
                            while ($row1 = mysql_fetch_array($result1)) {
                                ?>
                                <li class="text-center top10">
                                    <a class="btn btn-block rounded-0 <?php echo $active; ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/servicos/<?php Util::imprime($row1[url_amigavel]); ?>"
                                       title="<?php Util::imprime($row1[titulo]); ?>">
                                        <?php Util::imprime($row1[titulo]); ?>
                                    </a>
                                </li>
                                <?php
                            }
                        }
                        ?>

                        <li class="text-center borde_lateral top10 ">
                            <a class="btn btn-outline-secondary btn-block rounded-0 btn_outiline"
                               href="<?php echo Util::caminho_projeto() ?>/servicos" title="SAIBA MAIS">
                                VER TODOS
                            </a>
                        </li>
                    </ul>
            </div>

            <div class="seta_cat text-right">
                <div class="custom-navigation1">
                    <a href="#" class="flex-prev"><i class="fas fa-chevron-left fa-2x"></i></a>
                    <!-- <div class="custom-controls-container"></div> -->
                    <a href="#" class="flex-next"><i class="fas fa-chevron-right fa-2x"></i></a>
                </div>
            </div>

        </div>
    </div>





</div>
</div>
<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->
