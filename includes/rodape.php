


<div class="container-fluid rodape">
    <div class="row pb20">
        <div class="container">
            <div class="row">
                <div class="col-12 top25">

                    <!-- MENU   ================================================================ -->
                    <!-- ======================================================================= -->
                    <nav class="navbar  navbar-expand  navbar-light">

                        <div class=" navbar-collapse collapse " id="navbarNavDropdown">
                            <ul class="navbar-nav">

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "") {
                                        echo "active";
                                    } ?> "
                                       href="<?php echo Util::caminho_projeto() ?>/">HOME
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "empresa") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                                    </a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "servicos" or Url::getURL(0) == "servico") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "dicas" or Url::getURL(0) == "dica") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
                                    </a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "fale-conosco") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link <?php if (Url::getURL(0) == "orcamento") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "trabalhe-conosco") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO
                                    </a>
                                </li>


                            </ul>
                        </div>
                    </nav>
                    <!-- ======================================================================= -->
                    <!-- MENU    -->
                    <!-- ======================================================================= -->


                </div>

                <div class="col-12">
                    <div class="barra_branca"></div>
                </div>


                <!-- ======================================================================= -->
                <!-- LOGO    -->
                <!-- ======================================================================= -->
                <div class="col-3 text-center top15">
                    <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início"/>
                    </a>
                </div>
                <!-- ======================================================================= -->
                <!-- LOGO    -->
                <!-- ======================================================================= -->
                <div class="col-2 pl-0 top45">
                    <div class="btn btn-block btn_fale_rodape disabled">FALE CONOSCO</div>
                </div>

                <div class="col-5 p-0 top30">


                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                    <div class="col-10">
                        <div class="row  telefone_topo">


                            <div class="col-6 ">
                                <i class="fas fa-phone fa-fw right10 fa-lg" style="color: #000" data-fa-transform="rotate-90"></i>
                                <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                            </div>

                            <?php if (!empty($config[telefone2])): ?>
                                <div class="col-6 ">
                                    <i class="fab fa-whatsapp right10 fa-lg" style="color: #000"></i>
                                    <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                                </div>
                            <?php endif;

                            ?>
                            <?php if (!empty($config[telefone3])): ?>
                                <div class="col-6 top5">
                                    <i class="fas fa-phone fa-fw right10 fa-lg" style="color: #000" data-fa-transform="rotate-90"></i>
                                    <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
                                </div>
                            <?php endif;

                            ?>
                            <?php if (!empty($config[telefone4])): ?>
                                <div class="col-6 top5">
                                    <i class="fas fa-phone fa-fw right10 fa-lg" style="color: #000" data-fa-transform="rotate-90"></i>
                                    <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
                                </div>
                            <?php endif;

                            ?>
                        </div>


                    </div>

                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->

                    <?php if (!empty($config[endereco])): ?>
                        <div class="col-12 telefones top20">
                            <i class="fas fa-map-marker-alt mr-3"></i>
                             <?php Util::imprime($config[endereco]); ?>
                        </div>
                    <?php endif; ?>


                </div>


                <!-- ======================================================================= -->
                <!-- redes sociais    -->
                <!-- ======================================================================= -->
                <div class="col-2 top40 p-0 redes">
                    <?php if ($config[facebook] != "") { ?>
                        <a href="<?php Util::imprime($config[facebook]); ?>"  title="facebook" target="_blank">
                            <i class="fab fa-facebook-square fa-lg "></i>
                        </a>
                    <?php } ?>

                    <?php if ($config[instagram] != "") { ?>
                        <a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank">
                            <i class="fab fa-instagram fa-lg "></i>
                        </a>
                    <?php } ?>

                    <?php if ($config[google_plus] != "") { ?>
                        <a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus"
                           target="_blank">
                            <i class="fab fa-google-plus-g fa-lg "></i>
                        </a>
                    <?php } ?>

                    <a href="http://www.homewebbrasil.com.br" target="_blank">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png" alt="">
                    </a>

                </div>
                <!-- ======================================================================= -->
                <!-- redes sociais    -->
                <!-- ======================================================================= -->


            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row rodape_baixo">
        <div class="container">
            <div class="row ">
                <div class="col-12 text-center top5">
                    <h6> © Copyright DISKLIMPEZA</h6>
                </div>
            </div>
        </div>
    </div>
</div>
