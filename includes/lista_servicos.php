<?php if (Url::getURL(0) == "servicos") {
    $col = 'col-4';
} else {
    $col = 'col-3';
}
?>

<?php
if (mysql_num_rows($result1) > 0) {
    while ($row = mysql_fetch_array($result1)) {
        ?>

        <div class="<?php echo $col;?> servicos">
            <div class="card top35">
                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 268, 200, array("class" => "w-100", "alt" => "$row[titulo]")) ?>
                </a>
                <div class="card-body">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
                        <div class=" dica_line_titulo">
                            <h6 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-5 pr-0">
                    <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>"
                       class="btn btn-block btn-outline-secondary btn_detalhe_produtos rounded-0" role="button"
                       aria-pressed="true">
                        SAIBA MAIS
                    </a>
                </div>

                <div class="col-7">
                    <a href="javascript:void(0);" title="SOLICITAR UM ORÇAMENTO"
                       onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'"
                       class="btn btn-block btn-outline-success btn_detalhe_produtos btn_compra rounded-0" role="button"
                       aria-pressed="true">
                        ORÇAMENTO
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

