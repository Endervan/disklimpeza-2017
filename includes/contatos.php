<div class="container bottom50 top35">
    <div class="row">
        <div class="col-6 dicas_titulo fale_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($banner[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($banner[legenda_2]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0"></h6>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-6">
            <div class="row bg_cinza p-2">
                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->
                <div class="col-4 top5">
                    <div class="btn btn-block btn_atendimento disabled">LIGUE AGORA</div>
                </div>
                <div class="col-4 top10 telefone_topo text-center top5">
                    <i class="fas fa-phone fa-fw mr-3 fa-lg" data-fa-transform="rotate-90" style="color: #7f7f7f"></i>
                    <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                </div>

                <?php if (!empty($config[telefone2])): ?>
                    <div class="col-4 top10 text-center telefone_topo top5">
                        <i class="fab fa-whatsapp mr-3 fa-lg" style="color: #7f7f7f"></i>
                        <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                    </div>
                <?php endif;

                ?>



                <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>
            </div>
        </div>


    </div>
</div>
