


<div class="container-fluid  bg_topo">
    <div class="row">


        <div class="container ">
            <div class="row">
                <?php
                if (empty($voltar_para)) {
                    $link_topo = Util::caminho_projeto() . "/";
                } else {
                    $link_topo = Util::caminho_projeto() . "/" . $voltar_para;
                }
                ?>

                <div class="col-4 text-center top5">
                    <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $config[logomarca_desktop] ?>"
                             alt="início" class="">
                    </a>
                </div>


                <div class="col-8">
                    <div class="row">


                        <!-- ======================================================================= -->
                        <!-- telefones  -->
                        <!-- ======================================================================= -->
                        <div class="col-3 top10">
                            <div class="btn btn-block btn_atendimento disabled">ATENDIMENTO</div>
                        </div>
                        <div class="col-3 top15 telefone_topo text-center top5">
                            <i class="fas fa-phone fa-fw mr-3 fa-lg" data-fa-transform="rotate-90" style="color: #7f7f7f"></i>
                            <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                        </div>

                        
                            <div class="col-3 top15 text-center telefone_topo top5">
                                <?php if (!empty($config[telefone2])): ?>
                                    <i class="fab fa-whatsapp mr-3 fa-lg" style="color: #7f7f7f"></i>
                                    <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                                <?php endif; ?>
                            </div>
                        



                        <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>



                        <!--  ==============================================================  -->
                        <!--CARRINHO-->
                        <!--  ==============================================================  -->
                        <div class="col-3 top10 dropdown">
                            <a class="btn btn-outline-dark btn-block btn_atendimento" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ORÇAMENTO
                            </a>
                            <div class="dropdown-menu dropdown-menu-right topo-meu-orcamento tabela_carrinho" aria-labelledby="navbarDropdownMenuLink">
                                <table class="table  table-condensed">

                                    <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) == 0): ?>

                                       <div class="row">
                                           <div class="col-12 text-center"><h6 >NENHUMA ITEM SELECIONADO</h6></div>
                                           <div class="col-8">
                                               <div class="text-center bottom10">

                                                   <a href="<?php echo Util::caminho_projeto() ?>/servicos"
                                                      class="btn btn btn-outline-secondary rounded-0 btn_detalhe_produtos " >
                                                       ADICIONE ITEM
                                                   </a>
                                               </div>
                                           </div>
                                           <div class="col-4">
                                               <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco"
                                                  class="btn btn btn-outline-secondary rounded-0 btn_detalhe_produtos">
                                                   CONTATO
                                               </a>
                                           </div>
                                       </div>



                                    <?php else: ?>


                                        <?php
                                        if(count($_SESSION[solicitacoes_produtos]) > 0){
                                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                                                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                                ?>
                                                <tr class="middle">
                                                    <div class="row lista-itens-carrinho">
                                                        <div class="col-2 top5">
                                                            <?php if(!empty($row[imagem])): ?>
                                                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-8 top5">
                                                            <h6><?php Util::imprime($row[titulo]) ?></h6>
                                                        </div>
                                                        <div class="col-1">
                                                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                                        </div>
                                                        <div class="col-12"><div class="borda_carrinho top5">  </div> </div>
                                                    </div>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <!--  ==============================================================  -->
                                        <!--sessao adicionar servicos-->
                                        <!--  ==============================================================  -->
                                        <?php
                                        if(count($_SESSION[solicitacoes_servicos]) > 0)
                                        {
                                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                                            {
                                                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                                ?>
                                                <div class="row lista-itens-carrinho">
                                                    <div class="col-2 top5">
                                                        <?php if(!empty($row[imagem])): ?>
                                                            <!-- <img class="carrinho_servcos" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
                                                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-8 top5">
                                                        <h6><?php Util::imprime($row[titulo]) ?></h6>
                                                    </div>
                                                    <div class="col-1">
                                                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                                    </div>
                                                    <div class="col-12"><div class="borda_carrinho top5">  </div> </div>

                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <div class="col-12 text-right top10 bottom20">
                                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-block btn-outline-success btn_detalhe_produtos btn_compra rounded-0" >
                                                ENVIAR ORÇAMENTO
                                            </a>
                                        </div>

                                    <?php   endif; ?>

                                </table>

                            </div>
                        </div>
                        <!--  ==============================================================  -->
                        <!--CARRINHO-->
                        <!--  ==============================================================  -->


                    </div>


                    <div class="col-12 menu_topo p-0 text-center">

                        <!--  ==============================================================  -->
                        <!-- MENU-->
                        <!--  ==============================================================  -->
                        <nav class="navbar  navbar-expand  navbar-light p-0">

                            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                <ul class="navbar-nav col justify-content-end">

                                    <li class="nav-item <?php if (Url::getURL(0) == "") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/">
                                            <h6>HOME</h6>
                                            <span class="sr-only">(current)</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "empresa") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/empresa">
                                            <h6>A EMPRESA </h6>
                                        </a>
                                    </li>



                                    <li class="nav-item <?php if (Url::getURL(0) == "servicos" or Url::getURL(0) == "servico") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/servicos">
                                            <h6>SERVIÇOS</h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "dicas" or Url::getURL(0) == "dica") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/dicas">
                                            <h6>DICAS</h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "fale-conosco") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
                                            <h6>FALE CONOSCO</h6>
                                        </a>
                                    </li>


                            </div>


                    </div>


                </div>


            </div>

        </div>


    </div>

</div>


