<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url = $_GET[get1];

if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6 class="top5 "><?php Util::imprime($banner[legenda_1]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<div class="row">

    <div class="col-6 p-0">
        <amp-img
                src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>"
                width="180"
                height="103"
                layout="responsive"
                alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
    </div>
    <div class="col-6 top15 dica_dentro">
        <h6><?php echo Util::imprime($dados_dentro[titulo]) ?></h6>
        <div class="col-12 top5">
            <div class="border_titulo"></div>
        </div>
    </div>
    <div class="col-12 top20">
        <div><p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
    </div>


</div>


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->

<?php require_once("../includes/rodape.php") ?>

</body>


</html>
