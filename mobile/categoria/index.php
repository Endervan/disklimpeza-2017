<?php
$ids = explode("/", $_GET[get1]);

require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
            background-size: 100% 128px;
        }
    </style>


    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6><?php Util::imprime($banner[legenda_1]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->




<div class="row">


    <div class="col-12 menu_Cat top10">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action  active">
                Selecione Categoria
            </a>
            <?php

            $result = $obj_site->select("tb_categorias_servicos");
            if (mysql_num_rows($result) > 0) {
                $i = 0;
                while ($row1 = mysql_fetch_array($result)) {

                    ?>

                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row1[url_amigavel]); ?>"
                       class="list-group-item list-group-item-action" title="<?php Util::imprime($row1[titulo]); ?>">
                        <?php Util::imprime($row1[titulo]); ?>
                    </a>

                    <?php

                    if ($i == 1) {
                        echo '<div class="clearfix"></div>';
                        $i = 0;
                    } else {
                        $i++;
                    }

                }
            }
            ?>

            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/"
               class="list-group-item list-group-item-action">
                VER TODOS
            </a>

        </div>


    </div>

</div>



<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>


</html>
