<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



function adiciona($id, $tipo_orcamento){
  //  VERIFICO SE O TIPO DE SOLICITACAO E DE SERVICO OU PRODUTO
  switch($tipo_orcamento){
    case "servico":
    //	VERIFICO SE NAO TEM JA ADICIONADO
    if(count($_SESSION[solicitacoes_servicos] > 0)):
      if (!@in_array($id, $_SESSION[solicitacoes_servicos])) :
        $_SESSION[solicitacoes_servicos][] = $id;
      endif;
    else:
      $_SESSION[solicitacoes_servicos][] = $id;
    endif;
    break;


    case "produto":
    //	VERIFICO SE NAO TEM JA ADICIONADO
    if(count($_SESSION[solicitacoes_produtos] > 0)):
      if (!@in_array($id, $_SESSION[solicitacoes_produtos])) :
        $_SESSION[solicitacoes_produtos][] = $id;
      endif;
    else:
      $_SESSION[solicitacoes_produtos][] = $id;
    endif;
    break;
  }
}



function excluir($id, $tipo_orcamento){
  switch ($tipo_orcamento) {
    case 'produto':
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case 'servico':
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
  }
}


//  EXCLUI OU ADD UM ITEM
if($_GET[action] != ''){
  //  SELECIONO O TIPO
  switch($_GET[action]){
    case "add":
    adiciona($_GET[id], $_GET[tipo_orcamento]);
    break;
    case "del":
    excluir($_GET[id], $_GET[tipo_orcamento]);
    break;
  }
}


?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>






    <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>





  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>




</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>




  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  localizacao-pagina fundo_titulo">
          <h6 class="top5 "><?php Util::imprime($banner[legenda_1]); ?></h6>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->



  <?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_GET[nome])){
  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_GET[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_GET[idproduto][$i]);
    $produtos .= "
                    <tr>
                    <td><p>". $_GET[qtd][$i] ."</p></td>
                    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                    </tr>
                    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_GET[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_servicos", "idservico", $_GET[idservico][$i]);
    $produtos .= "
                    <tr>
                    <td><p>". $_GET[qtd_servico][$i] ."</p></td>
                    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                    </tr>
                    ";
  }


  //  ENVIANDO A MENSAGEM PARA O CLIENTE
   $texto_mensagem = "
                      O seguinte cliente fez uma solicitação pelo site. <br />

                      Assunto: ".($_GET[assunto])." <br />
                      Nome: ".($_GET[nome])." <br />
                      Email: ".($_GET[email])." <br />
                      Telefone: ".($_GET[telefone])." <br />
                      Celular: ".($_GET[celular])." <br />

                      Mensagem: <br />
                      ". nl2br($_GET[mensagem]) ." <br />


                      <h2> Produtos selecionados:</h2> <br />
                      <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                      <tr>
                      <td><h4>QTD</h4></td>
                      <td><h4>PRODUTO</h4></td>
                      </tr>
                      $produtos
                      </table>
                      ";


  if (Util::envia_email($config[email_orcamento],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email])) {
      Util::envia_email($config[email_copia],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email]);
      unset($_SESSION[solicitacoes_produtos]);
      unset($_SESSION[solicitacoes_servicos]);
      $enviado = 'sim';
  }




}
?>




  <div class="row bottom50 fundo_contatos ">
    <div class="col-12">

      <?php if($enviado == 'sim'): ?>
          <div class="col-12  text-center top40">
              <h4>Orçamento enviado com sucesso.</h4>
              <a class="btn btn_escuro" href="<?php echo Util::caminho_projeto() ?>/mobile/categorias">
                ADICIONE SERVIÇO(S)
              </a>
          </div>
      <?php else: ?>

          <form method="get" class="p2" action-xhr="envia.php" target="_top">

        <?php require_once('../includes/lista_itens_orcamento.php'); ?>

        <!-- <div class="lista-produto-titulo top20">
          <h5 >CONFIRME SEUS DADOS</h5>
        </div> -->

        <div class="ampstart-input inline-block form_contatos top30">

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
            <span class="fa fa-user-circle form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
            <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none" name="fala" placeholder="CELULAR">
            <span class="fas fa-mobile form-control-feedback"></span>
          </div>


          <div class="relativo">
            <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
            <span class="fas fa-edit form-control-feedback"></span>
          </div>

        </div>
        <div class="text-right">
          <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) > 0): ?>
            <input type="submit" value="ENVIAR ORÇAMENTO" class="btn btn-lg btn-block top30 btn_topo btn_orcamento btn_orcamento_servico">

          <?php else: ?>
            <h6 class="col-12 text-center ">*OBS : ADICIONE ITEM PARA ENVIAR ORÇAMENTO</h6>

          <?php endif; ?>
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Orçamento enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    <?php endif; ?>
    </div>
  </div>


  <div class="row">
      <div class="col-12">


          <a class="btn btn-lg btn-block btn_topo btn_ligue top5"
             on="tap:my-lightbox444"
             role="a"
             tabindex="0">
              LIGUE AGORA
          </a>

      </div>


      <div class="col-12 p-0 top20 contato_emp">

          <div class="col-6  media">
              <i class="fas fa-phone fa-lg align-self-center mr-1" data-fa-transform="rotate-90"></i>
              <div class="media-body">
                  <h6 class="mt-0"><span>Atendimento</span></h6>
                  <a href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
                      <h6 class="font-weight-bold"><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
                  </a>
              </div>
          </div>

          <div class="col-6  media">
              <i class="fab fa-whatsapp fa-lg align-self-center mr-1"></i>
              <div class="media-body">
                  <h6 class="mt-0"><span>Whatsapp</span></h6>
                  <a href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">

                      <h6 class="font-weight-bold"><?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
                  </a>
              </div>
          </div>
      </div>
  </div>


  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->
  <div class="row bottom40 top10">
      <div class="col-12 padding0">
          <amp-iframe
                  width="480"
                  height="300"
                  layout="responsive"
                  sandbox="allow-scripts allow-same-origin allow-popups"
                  frameborder="0"
                  src="<?php Util::imprime($config[src_place]); ?>">
          </amp-iframe>

      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->



   <?php require_once("../includes/rodape.php") ?>

</body>

</html>
