<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }


    </style>


</head>

<body class="bg-interna">


<?php require_once("../includes/topo.php") ?>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6 class="top5 "><?php Util::imprime($banner[legenda_1]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<div class="row bottom30 fundo_contatos">
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12">


        <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if (isset($_POST[nome])) {
            $texto_mensagem = "
        Assunto: " . ($_POST[assunto]) . " <br />
        Nome: " . ($_POST[nome]) . " <br />
        Email: " . ($_POST[email]) . " <br />
        Telefone: " . ($_POST[telefone]) . " <br />
       Celular: " . ($_POST[celular]) . " <br />


        Mensagem: <br />
        " . (nl2br($_POST[mensagem])) . "
        ";

            if (Util::envia_email($config[email_trabalhe_conosco], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem),
                ($_POST[nome]), $_POST[email])) {
                    Util::envia_email($config[email_copia], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem),
                    ($_POST[nome]), $_POST[email]);
                $enviado = 'sim';
                unset($_POST);
            }


        }
        ?>


        <?php if ($enviado == 'sim'): ?>
            <div class="col-12  text-center top40">
                <h4>Email enviado com sucesso.</h4>
                <a class="btn btn_escuro" href="<?php echo Util::caminho_projeto() ?>/mobile/contato">
                    ENVIE OUTRO EMAIL
                </a>
            </div>
        <?php else: ?>


            <form method="post" class="p2" action-xhr="index.php" target="_top">


                <div class="ampstart-input top10 inline-block">

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="assunto"
                               placeholder="ASSUNTO" required>
                        <span class="fa fa-user-circle form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="nome" placeholder="NOME"
                               required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="email" class="input-form input100 block border-none" name="email"
                               placeholder="EMAIL" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="tel" class="input-form input100 block border-none" name="telefone"
                               placeholder="TELEFONE" required>
                        <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="celular"
                               placeholder="CELULAR">
                        <span class="fa fas fa-mobile-alt form-control-feedback"></span>
                    </div>


                    <div class="relativo">
                        <textarea name="mensagem" placeholder="MENSAGEM"
                                  class="input-form input100 campo-textarea"></textarea>
                        <span class="fas fa-pencil-alt form-control-feedback"></span>
                    </div>

                </div>

                <div class="col-12 padding0">
                    <div class="relativo ">
                        <button type="submit"
                                value="OK"
                                class="btn btn-lg btn-block top30 btn_topo btn_orcamento btn_orcamento_servico">
                            ENVIAR MENSAGEM
                        </button>
                    </div>
                </div>


                <div submit-success>
                    <template type="amp-mustache">
                        Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
                    </template>
                </div>

                <div submit-error>
                    <template type="amp-mustache">
                        Houve um erro, {{name}} por favor tente novamente.
                    </template>
                </div>

            </form>

        <?php endif; ?>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->

</div>

<div class="row">
    <div class="col-12">


        <a class="btn btn-lg btn-block btn_topo btn_ligue top5"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            LIGUE AGORA
        </a>

    </div>


    <div class="col-12 p-0 top20 contato_emp">

        <div class="col-6  media">
            <i class="fas fa-phone fa-lg align-self-center mr-1" data-fa-transform="rotate-90"></i>
            <div class="media-body">
                <h6 class="mt-0"><span>Atendimento</span></h6>
                <a href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
                    <h6 class="font-weight-bold"><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
                </a>
            </div>
        </div>

        <div class="col-6  media">
            <i class="fab fa-whatsapp fa-lg align-self-center mr-1"></i>
            <div class="media-body">
                <h6 class="mt-0"><span>Whatsapp</span></h6>
                <a href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">

                    <h6 class="font-weight-bold"><?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
                </a>
            </div>
        </div>
    </div>
</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row bottom40 top10">
    <div class="col-12 padding0">
        <amp-iframe
                width="480"
                height="300"
                layout="responsive"
                sandbox="allow-scripts allow-same-origin allow-popups"
                frameborder="0"
                src="<?php Util::imprime($config[src_place]); ?>">
        </amp-iframe>

    </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
