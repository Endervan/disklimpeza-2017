<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }
    </style>


</head>

<body class="bg-interna">


<?php require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6 class="top5 "><?php Util::imprime($row[titulo]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!--   EMPRESA -->
<!--  ==============================================================  -->
<div class="row empresa_geral">

    <div class="col-12 top30">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
        <div><p><?php Util::imprime($row[descricao]); ?></p></div>

        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <div><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
</div>
<!--  ==============================================================  -->
<!--   EMPRESA -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
