<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }
    </style>

</head>

<body class="bg-interna">


<?php
$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6 class="top5 "><?php Util::imprime($banner[legenda_1]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->

<div class="row">
    <?php
    $i = 0;
    $result = $obj_site->select("tb_dicas");
    if (mysql_num_rows($result) > 0) {
    while ($row = mysql_fetch_array($result)) {
    ?>

    <div class="col-6 relativo dicas top20">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>"
           title="<?php Util::imprime($row[titulo]); ?>">
            <div class="card">
                <amp-img
                        src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
                        width="150"
                        height="100"
                        layout="responsive"
                        alt="<?php echo Util::imprime($row[titulo]) ?>">
                </amp-img>
                <div class="card-body top5">
                    <div class=" dica_line_titulo">
                        <h6 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h6>
                    </div>
                </div>
            </div>
        </a>
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>"
           class="btn btn_detalhe_dicas " role="button" aria-pressed="true">
            <div class="media">
                <i class="fas fa-plus-circle fa-2x" style="color: #0b8a3f;"></i>
                <div class=" align-self-center">
                </div>
            </div>
        </a>
    </div>
<?php

}
}
?>
</div>


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
