<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("./includes/head.php"); ?>

    <style amp-custom>
        <?php require_once("./css/geral.css"); ?>
        <?php require_once("./css/topo_rodape.css"); ?>
        <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        .bg-interna {
            background: #eaeae9 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_index]); ?>) top center no-repeat;
            -webkit-background-size: 100%;
            -moz-background-size: 100%;
            -o-background-size: 100%;
            background-size: 100%;
        }
    </style>


</head>


<body class="bg-interna">

<div class="row ">
    <div class="col-12 bg-branco text-center topo pt5 bottom5">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_paginas]) ?>"
                 alt="Home"
                 height="60"
                 width="150">
        </amp-img>
    </div>
</div>


<div class=" row font-index text-center">
    <div class="col-12 padding0 top25">
        <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 3); ?>
        <h4 class="text-center"><?php Util::imprime($row1[legenda]); ?></h4>

        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_home.png"
                 width="210"
                 height="2"
                 alt="AMP">
        </amp-img>

    </div>
</div>


<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->
<div class="row">

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_geral_servicos.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dicas">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_dicas.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>



    <div class="col-4 col-offset-2 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contato">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>





    <div class="col-4 top30">
        <a href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>


</div>
<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->


<?php require_once("./includes/rodape.php") ?>


</body>


</html>
