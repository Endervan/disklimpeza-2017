<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url = $_GET[get1];

if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        /*
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }
        */
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>




<div class="row">





    <div class="col-12 top25 dica_dentro">
        <h6><?php echo Util::imprime($dados_dentro[titulo]) ?></h6>
        <div class="col-12 top20 bottom10">
            <div class="border_titulo"></div>
        </div>
    </div>


        <div class="col-12 padding0 ">

            <?php
            $result = $obj_site->select("tb_galerias_servicos", "AND id_servico = '$dados_dentro[0]' ");
            if (mysql_num_rows($result) == 0) { ?>

                <amp-carousel id="carousel-with-preview"
                            width="360"
                            height="280"
                            layout="responsive"
                            type="slides"
                            autoplay
                            delay="4000"
                >

                    <amp-img
                            on="tap:lightbox2"
                            role="button"
                            tabindex="0"
                            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
                            layout="responsive"
                            width="50"
                            height="50"
                            alt="default">

                    </amp-img>
                </amp-carousel>

                <amp-image-lightbox id="lightbox2" layout="nodisplay"></amp-image-lightbox>

            <?php } else {
                ?>
                <amp-carousel id="carousel-with-preview"
                            width="360"
                            height="280"
                            layout="responsive"
                            type="slides"
                            autoplay
                            delay="8000"
                >

                    <?php
                    $i = 0;

                    if (mysql_num_rows($result) > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            ?>

                            <amp-img
                                    on="tap:lightbox1"
                                    role="button"
                                    tabindex="0"

                                    src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
                                    layout="responsive"
                                    width="360"
                                    height="280"
                                    alt="<?php echo Util::imprime($row[titulo]) ?>">

                            </amp-img>
                            <?php
                        }
                        $i++;
                    }


                    ?>

                </amp-carousel>

                <amp-image-lightbox id="lightbox1" layout="nodisplay"></amp-image-lightbox>

            <?php } ?>

            </div>





    <div class="col-12 top20">
        <a class="btn btn-lg btn-block btn_topo btn_orcamento btn_orcamento_servico" title="SOLICITAR UM ORÇAMENTO"
           href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico">
            SOLIÇITAR ORÇAMENTO
        </a>
    </div>


    <div class="col-12 top20">
        <div><p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
    </div>



</div>


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->

<?php require_once("../includes/rodape.php") ?>

</body>


</html>
