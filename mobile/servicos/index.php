<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 128px center no-repeat;
            -webkit-background-size: 100% 128px;
            -moz-background-size: 100% 128px;
            -o-background-size: 100% 128px;
            background-size: 100% 128px;
        }
    </style>

</head>

<body class="bg-interna">


<?php
$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
    <div class="col-12  localizacao-pagina fundo_titulo">
        <h6 class="top5 "><?php Util::imprime($banner[legenda_1]); ?></h6>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->

<div class="row">

    <?php
    $ids = explode("/", $_GET[get1]);


    //  FILTRA AS CATEGORIAS
    if (!empty( $ids[0] )) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_servicos", "idcategoriaservico", $ids[0]);
        $complemento .= "AND id_categoriaservico = '$id_categoria' ";
    }




    //  FILTRA PELO TITULO
    if(isset($_POST[busca_produtos])):
        $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
    endif;

    ?>



    <?php

    //  busca os produtos sem filtro
    $result = $obj_site->select("tb_servicos", $complemento);

    if(mysql_num_rows($result) == 0){
        echo "<h5 class=' text-center  top20 clearfix'>Nenhum Serviço encontrado.</h5>";
    }else{
        ?>

        <?php if (isset($_GET[get1]) > 0):?>
        <div class="col-12 total-resultado-busca top10 bottom10">
            <h1><?php echo mysql_num_rows($result) ?> SERVIÇO(S) ENCONTRADO(S) .</h1>
        </div>
            <?php endif; ?>
        <?php
        require_once('../includes/lista_servicos.php');

    }
    ?>

</div>

<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
