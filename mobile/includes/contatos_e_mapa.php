<div class="row">
    <div class="col-12 top40">


        <?php $ids = explode("/", $_GET[get1]);
        if (isset($_GET[get1]) > 0):?>
            <a class="btn btn-lg btn-block top30 btn_topo btn_orcamento btn_orcamento_servico"
               title="SOLICITAR UM ORÇAMENTO"
               href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico">
                SOLIÇITAR ORÇAMENTO
            </a>
        <?php else: ?>
            <a class="btn btn-lg btn-block btn_topo btn_orcamento top5"
               href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento"
               on="tap:my-lightbox444"
               role="a"
               tabindex="0">
                ORÇAMENTO
            </a>
        <?php endif; ?>

        <a class="btn btn-lg btn-block btn_topo btn_ligue top5"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            LIGUE AGORA
        </a>

    </div>


    <div class="col-12 p-0 top20 contato_emp">

        <div class="col-6  media">
            <i class="fas fa-phone fa-lg align-self-center mr-1" data-fa-transform="rotate-90"></i>
            <div class="media-body">
                <h6 class="mt-0"><span>Atendimento</span></h6>
                <a href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
                    <h6 class="font-weight-bold"><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
                </a>
            </div>
        </div>

        <div class="col-6  media">
            <?php if(!empty($row[ddd2]) and !empty($row[telefone2]) ): ?>
                <i class="fab fa-whatsapp fa-lg align-self-center mr-1"></i>
                <div class="media-body">
                    <h6 class="mt-0"><span>Whatsapp</span></h6>
                    <a href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">

                        <h6 class="font-weight-bold"><?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row bottom40 top10">
    <div class="col-12 padding0">
        <amp-iframe
                width="480"
                height="300"
                layout="responsive"
                sandbox="allow-scripts allow-same-origin allow-popups"
                frameborder="0"
                src="<?php Util::imprime($config[src_place]); ?>">
        </amp-iframe>

    </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
