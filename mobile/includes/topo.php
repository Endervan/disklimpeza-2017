

<?php
$obj_usuario = new Usuario();


?>



<div class="row bg_topo">

    <?php
    if(empty($voltar_para)){
        $link_topo = Util::caminho_projeto()."/mobile/";
    }else{
        $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
    }
    ?>

    <div class="col-3 top15">
        <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>

    </div>
    <div class="col-6 text-center top5">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile">
            <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_paginas]) ?> "
                     alt="Home" height="50" width="126"
            class="input100">

            </amp-img>
        </a>
    </div>

    <div class="col-3 top15 text-right">
        <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
    </div>

</div>

<div class="row top15">
    <div class="col-6">
        <a class="btn btn-block btn_cat" href="<?php echo Util::caminho_projeto() ?>/mobile/categoria">
            CATEGORIAS <i class="fas fa-caret-down left10"></i>
        </a>

    </div>

    <div class="col-6">
        <a class="btn btn-block btn_topo"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            <i class="fas fa-phone " data-fa-transform="rotate-90"></i> LIGUE AGORA
        </a>
    </div>


</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
    <ul class="menu-mobile">
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fas fa-home" aria-hidden="true"></i> Home</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fas fa-building"></i> A Empresa</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos"> <i class="far fa-handshake"></i> Serviços</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas"> <i class="far fa-thumbs-up"></i> Dicas</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/contato"> <i class="fa fa-envelope" aria-hidden="true"></i> Contato</a></li>
    </ul>



</amp-sidebar>
