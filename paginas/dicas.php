<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>



    </div>
</div>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom120">

        <div class="col-11 ml-auto titulo_internas">
            <h1><?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CATEGORIA   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/lista_categoria.php') ?>
<!-- ======================================================================= -->
<!-- CATEGORIA    -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- DICAS -->
<!-- ======================================================================= -->
<div class="container bottom70 top35">
    <div class="row">
        <div class="col-12 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($banner[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($banner[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0"><?php Util::imprime($banner[legenda_2]); ?></h6>
                    </div>
                </div>


            </div>

        </div>

        <?php $result = $obj_site->select("tb_dicas");
        require_once('./includes/lista_dicas.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- servicos -->
<!-- ======================================================================= -->
<div class="container bottom70 top50">
    <div class="row">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($row[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0"><?php Util::imprime($row[legenda_2]); ?></h6>
                    </div>
                </div>


            </div>

        </div>
        <div class="col-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos"
               class="btn  btn-lg btn-block btn-outline-secondary  rounded-0 btn_servicos  col-6 ml-auto" role="button" aria-pressed="true">
                VER TODOS OS SERVIÇOS
            </a>
        </div>

        <?php $result1 = $obj_site->select("tb_servicos", "order by rand() limit 4");
        require_once('./includes/lista_servicos.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- servicos    -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
    <?php /*
    $(window).load(function () {
        $('#slider_carousel').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas
            animationLoop: true,
            itemWidth: 195,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20

        });
    });

    */ ?>

    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 220,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });


</script>

