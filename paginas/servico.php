<?php
// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom120">

        <div class="col-11 ml-auto titulo_internas">
            <h1><?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CATEGORIA   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/lista_categoria.php') ?>
<!-- ======================================================================= -->
<!-- CATEGORIA    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CONTATO -->
<!-- ======================================================================= -->
<div class="container bottom50 top35">
    <div class="row">
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h6 class="top7 mr-2"> <?php Util::imprime($banner[legenda_1]); ?> :
                        <span><?php Util::imprime(Util::troca_value_nome($dados_dentro[id_categoriaservico], "tb_categorias_servicos", "idcategoriaservico", "titulo")); ?></span>
                    </h6>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                    </div>
                </div>


            </div>

        </div>

        <div class="col-6">
            <div class="row bg_cinza p-2">
                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->
                <div class="col-4 top5">
                    <div class="btn btn-block btn_atendimento disabled">ATENDIMENTO</div>
                </div>
                <div class="col-4 top10 telefone_topo text-center top5">
                    <i class="fas fa-phone fa-fw mr-3 fa-lg" data-fa-transform="rotate-90" style="color: #7f7f7f"></i>
                    <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                </div>

                <?php if (!empty($config[telefone2])): ?>
                    <div class="col-4 top10 text-center telefone_topo top5">
                        <i class="fab fa-whatsapp mr-3 fa-lg" style="color: #7f7f7f"></i>
                        <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                    </div>
                <?php endif;

                ?>



                <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>
            </div>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- CONTATO    -->
<!-- ======================================================================= -->


<div class="container top40">
    <div class="row">


        <div class="col-6 p-0">

            <!-- ======================================================================= -->
            <!-- SLIDER    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_produto_servico_dentro.php') ?>
            <!-- ======================================================================= -->
            <!-- SLIDER    -->
            <!-- ======================================================================= -->

        </div>

        <div class="col-6 order-12  servico_Dentro">
            <h2><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h2>
            <div class="top30">
                <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
            </div>
            <a href="javascript:void(0);" class="btn col-8 btn-outline-success btn_detalhe_produtos btn_compra rounded-0 top60" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
                SOLICITE ORÇAMENTO
            </a>
        </div>

    </div>


</div>


<!-- ======================================================================= -->
<!-- servicos -->
<!-- ======================================================================= -->
<div class="container bottom70 top50">
    <div class="row">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> OUTROS</h4>
                    <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0">CONFIRA MAIS</h6>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos"
               class="btn  btn-lg btn-block btn-outline-secondary  rounded-0 btn_servicos  col-6 ml-auto" role="button"
               aria-pressed="true">
                VER TODOS OS SERVIÇOS
            </a>
        </div>

        <?php $result1 = $obj_site->select("tb_servicos", "order by rand() limit 4");
        require_once('./includes/lista_servicos.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- servicos    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">


    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 220,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });
</script>



<script type="text/javascript">
    $(window).load(function() {


        $('#carousel_item').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            itemWidth: 110,
            itemMargin: 0,
            asNavFor: '#slider_item'
        });

        $('#slider_item').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            sync: "#carousel_item"
        });


    });

</script>
