<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if (isset($_GET[action])) {
    //  SELECIONO O TIPO
    switch ($_GET[tipo]) {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
            break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
            break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
            break;
    }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>


</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom65">

        <div class="col-11 ml-auto titulo_internas">
            <h1><?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- MENU CONTATOS    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row barra_fale">
        <div class="col-7">
            <ul class=" nav nav-pills top10 justify-content-center">
                <li class="nav-item nav-block col text-center">
                    <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco"
                       class=" nav-link active barra_fale_border">
                        FALE CONOSCO
                    </a>
                </li>
                <li class="nav-item col-5 text-center">
                    <a class=" nav-link barra_fale_border"
                       href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                        TRABALHE CONOSCO
                    </a>
                </li>
                <li class="nav-item col text-center">
                    <a class="nav-link" href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
                        ONDE ESTAMOS
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- MENU CONTATOS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CONTATO -->
<!-- ======================================================================= -->
<?php require_once('./includes/contatos.php') ?>
<!-- ======================================================================= -->
<!-- CONTATO    -->
<!-- ======================================================================= -->


<div class="container bottom40">
    <form class="FormContatos needs-validation " novalidate role="form" method="post"
          enctype="multipart/form-data">
        <div class="row ">


            <div class="col-5">

                <!-- ======================================================================= -->
                <!-- ITENS CARRINHO  -->
                <!-- ======================================================================= -->
                <?php require_once('./includes/lista_itens_orcamento.php') ?>
                <!-- ======================================================================= -->
                <!-- ITENS CARRINHO  -->
                <!-- ======================================================================= -->
            </div>


            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->
            <div class="col-7 fundo_formulario">

                <div class="form-row">
                    <div class="col">
                        <div class="form-group relativo">
                            <input type="text" name="assunto" class="form-control fundo-form"
                                   placeholder="ASSUNTO" required>
                            <span class="fas fa-user-circle form-control-feedback "></span>
                            <div class="invalid-feedback">
                                Adicione Assunto.
                            </div>

                            <div class="col-12 text-right valid-feedback">
                                <i class="fas fa-check-circle" aria-hidden></i>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="form-row">
                    <div class="col">
                        <div class="form-group relativo">
                            <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME"
                                   required>
                            <span class="fa fa-user form-control-feedback"></span>
                            <div class="invalid-feedback">
                                Por favor Insira um Nome
                            </div>
                            <div class="col-12 text-right valid-feedback">
                                <i class="fas fa-check-circle" aria-hidden></i>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group relativo">
                            <input type="email" name="email" class="form-control fundo-form"
                                   placeholder="E-MAIL" required>
                            <span class="fa fa-envelope form-control-feedback"></span>
                            <div class="invalid-feedback">
                                Informe Email Válido
                            </div>
                            <div class="col-12 text-right valid-feedback">
                                <i class="fas fa-check-circle" aria-hidden></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <div class="form-group relativo">

                            <input type="tel" name="telefone" id="telefone" class="form-control fundo-form"
                                   placeholder="TELEFONE" required>
                            <span class="fa fa-phone form-control-feedback"
                                  data-fa-transform="rotate-90"></span>
                            <div class="invalid-feedback">
                                seu número
                            </div>
                            <div class="col-12 text-right valid-feedback">
                                <i class="fas fa-check-circle" aria-hidden></i>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group relativo">
                            <input type="tel" name="celular" id="celular" class="form-control fundo-form"
                                   placeholder="CELULAR">
                            <span class="fa fas fa-mobile form-control-feedback"></span>
                        </div>
                    </div>
                </div>


                <div class="form-row">
                    <div class="col">
                        <div class="form-group  relativo">
                                    <textarea name="mensagem" cols="25" rows="5" class="form-control fundo-form"
                                              placeholder="MENSAGEM" required></textarea>
                            <span class="fas fa-pencil-alt form-control-feedback"></span>
                            <div class="invalid-feedback">
                                Escreva Uma Mensagem
                            </div>
                            <div class="col-12 text-right valid-feedback">
                                <i class="fas fa-check-circle" aria-hidden></i>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (count($_SESSION[solicitacoes_produtos]) + count($_SESSION[solicitacoes_servicos]) > 0) : ?>
                    <div class="col-12 mont text-right padding0 ">
                        <button type="submit" class="btn btn-outline-dark btn_atendimento top20"
                                name="btn_contato">
                            ENVIAR
                        </button>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-9 top10 mont text-right padding0 ">
                            <span>***OBG : PARA ENVIAR ORÇAMENTO ADICIONE UM ITEM ***</span>
                        </div>
                        <div class="col-3 text-right">
                            <button disabled class="btn btn-outline-dark btn_atendimento top20 "
                                    name="btn_contato">
                                ENVIAR
                            </button>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->


        </div>
    </form>

</div>


<!-- ======================================================================= -->
<!-- ONDE ESTAMOS   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/mapa.php') ?>
<!-- ======================================================================= -->
<!-- ONDE ESTAMOS    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if (isset($_POST[nome])) {


    //  CADASTRO OS PRODUTOS SOLICITADOS
    for ($i = 0; $i < count($_POST[qtd]); $i++) {
        $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

        $produtos .= "
    <tr>
    <td><p>" . $_POST[qtd][$i] . "</p></td>
    <td><p>" . utf8_encode(($dados[titulo])) . "</p></td>
    </tr>
    ";
    }

    //  CADASTRO OS SERVICOS SOLICITADOS
    for ($i = 0; $i < count($_POST[qtd_servico]); $i++) {
        $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
        $produtos .= "
    <tr>
    <td><p>" . $_POST[qtd_servico][$i] . "</p></td>
    <td><p>" . utf8_encode(($dados[titulo])) . "</p></td>
    </tr>
    ";
    }


    //  ENVIANDO A MENSAGEM PARA O CLIENTE
    echo $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Assunto: " . ($_POST[assunto]) . " <br />
  Nome: " . ($_POST[nome]) . " <br />
  Email: " . ($_POST[email]) . " <br />
  Telefone: " . ($_POST[telefone]) . " <br />
  Celular: " . ($_POST[celular]) . " <br />


  Mensagem: <br />
  " . nl2br($_POST[mensagem]) . " <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  </table>

  ";


    if (Util::envia_email($config[email_orcamento], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), ($_POST[email]))) {
        Util::envia_email($config[email_copia], ("$_POST[nome] solicitou um orçamento"), ($texto_mensagem), ($_POST[nome]), ($_POST[email]));
        unset($_SESSION[solicitacoes_produtos]);
        unset($_SESSION[solicitacoes_servicos]);
        unset($_SESSION[piscinas_vinil]);
        Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
    } else {
        Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
    }

}


?>
