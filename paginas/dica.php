<?php
// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom120">

        <div class="col-11 ml-auto titulo_internas">
            <h1><?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CATEGORIA   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/lista_categoria.php') ?>
<!-- ======================================================================= -->
<!-- CATEGORIA    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CONTATO -->
<!-- ======================================================================= -->
<div class="container bottom50 top35">
    <div class="row">
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($banner[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($banner[legenda_2]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0">ÚTEIS PARA VOCÊ</h6>
                    </div>
                </div>


            </div>

        </div>

            <div class="col-6">
                <div class="row bg_cinza p-2">
                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                    <div class="col-4 top5">
                        <div class="btn btn-block btn_atendimento disabled">ATENDIMENTO</div>
                    </div>
                    <div class="col-4 top10 telefone_topo text-center top5">
                        <i class="fas fa-phone fa-fw mr-3 fa-lg" data-fa-transform="rotate-90" style="color: #7f7f7f"></i>
                        <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                    </div>

                    <?php if (!empty($config[telefone2])): ?>
                        <div class="col-4 top10 text-center telefone_topo top5">
                            <i class="fab fa-whatsapp mr-3 fa-lg" style="color: #7f7f7f"></i>
                            <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                        </div>
                    <?php endif;

                    ?>



                    <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>
                </div>
            </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- CONTATO    -->
<!-- ======================================================================= -->



<div class="container top40">
    <div class="row">
        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-9 descricao_interna">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 848, 280, array("class" => "w-100 bottom20", "alt" => "$row[titulo]")) ?>
            <div class="col-8 position_dica bg_branco top20">
                <h2><?php Util::imprime($dados_dentro[titulo]); ?></h2>
            </div>
            <div class=" top30"><p><?php Util::imprime($dados_dentro[descricao]); ?></p></div>

            <div class="col-10 top120 linha_cinza"></div>
        </div>
        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- OUTRAS DICAS    -->
        <!-- ======================================================================= -->
        <div class="col-3 p-0">
            <?php $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
            $i = 0;
            if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                    ?>
                    <div class="col-12 dicas bottom20">
                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                            <div class="card">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 200, array("class" => "card-img-top", "alt" => "$row[titulo]")) ?>
                                <div class="card-body">
                                    <div class=" dica_line_titulo">
                                        <h6 ><?php Util::imprime($row[titulo]); ?></h6>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="col-12 top20">
                            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"
                               class="btn btn_detalhe_dicas " role="button" aria-pressed="true">
                                <i class="fas fa-plus-circle fa-4x"></i>
                            </a>
                        </div>
                    </div>
                    <?php

                }
            }
            ?>

        </div>
        <!-- ======================================================================= -->
        <!-- OUTRAS DICAS    -->
        <!-- ======================================================================= -->



    </div>


</div>


<!-- ======================================================================= -->
<!-- servicos -->
<!-- ======================================================================= -->
<div class="container bottom70 top50">
    <div class="row">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> OUTROS</h4>
                    <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0">CONFIRA MAIS</h6>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos"
               class="btn  btn-lg btn-block btn-outline-secondary  rounded-0 btn_servicos  col-6 ml-auto" role="button" aria-pressed="true">
                VER TODOS OS SERVIÇOS
            </a>
        </div>

        <?php $result1 = $obj_site->select("tb_servicos", "order by rand() limit 4");
        require_once('./includes/lista_servicos.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- servicos    -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
    <?php /*
    $(window).load(function () {
        $('#slider_carousel').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas
            animationLoop: true,
            itemWidth: 195,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20

        });
    });

    */ ?>

    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 220,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });


</script>

