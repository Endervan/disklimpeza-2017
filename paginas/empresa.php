<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>



    </div>
</div>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom120">

        <div class="col-11 ml-auto titulo_internas">
            <h1> A <?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CATEGORIA   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/lista_categoria.php') ?>
<!-- ======================================================================= -->
<!-- CATEGORIA    -->
<!-- ======================================================================= -->


<div class="container-fluid fundo_empresa">
    <div class="row">
        <div class="container">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
            <div class="row dicas_titulo">
                <div class="col-11 ml-auto top40 ">
                    <div class="row pl-3">
                        <h4 class="top7 mr-2"> <?php Util::imprime($row[titulo]); ?></h4>
                        <h2><?php Util::imprime($row[legenda]); ?></h2>
                    </div>
                </div>
                <div class="col-11 ml-auto">
                    <div class="media">
                        <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                        <div class="media-body">
                            <h6 class="mt-0"><?php Util::imprime($row[titulo]); ?><?php Util::imprime($row[legenda]); ?></h6>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                    <p><?php Util::imprime($row1[descricao]); ?></p>
                </div>

                <div class="col-12">
                    <p><?php Util::imprime($row[descricao]); ?></p>
                </div>

            </div>
        </div>
    </div>
</div>

<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<?php require_once('./includes/mapa.php') ?>
<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">

    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 220,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });


</script>