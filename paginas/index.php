<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>
</head>
<body>


<div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
</div>

<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->
<div class="container-fluid relativo">
    <div class="row">
        <div id="container_banner">
            <div id="content_slider">
                <div id="content-slider-1" class="contentSlider rsDefault">

                    <?php
                    $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
                    if (mysql_num_rows($result) > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <!-- ITEM -->
                            <div>
                                <?php
                                if ($row[url] == '') {
                                    ?>
                                    <img class="rsImg"
                                         src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                         data-rsw="1920" data-rsh="996" alt=""/>
                                    <?php
                                } else {
                                    ?>
                                    <a href="<?php Util::imprime($row[url]) ?>">
                                        <img class="rsImg"
                                             src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                             data-rsw="1920" data-rsh="996" alt=""/>
                                    </a>
                                    <?php
                                }
                                ?>

                            </div>
                            <!-- FIM DO ITEM -->
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>


        <!-- ======================================================================= -->
        <!-- CATEGORIA   =====================================================-->
        <!-- ======================================================================= -->
        <?php require_once('./includes/lista_categoria.php') ?>
        <!-- ======================================================================= -->
        <!-- CATEGORIA    -->
        <!-- ======================================================================= -->

</div>
</div>
<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- servicos -->
<!-- ======================================================================= -->
<div class="container bottom70 top50">
    <div class="row">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
        <div class="col-6 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($row[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0"><?php Util::imprime($row[legenda_2]); ?></h6>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos"
               class="btn  btn-lg btn-block btn-outline-secondary  rounded-0 btn_servicos  col-6 ml-auto" role="button" aria-pressed="true">
                VER TODOS OS SERVIÇOS
            </a>
        </div>

        <?php $result1 = $obj_site->select("tb_servicos", "and exibir_home='SIM'order by rand() limit 8");
        require_once('./includes/lista_servicos.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- servicos    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- EMPRESA -->
<!-- ======================================================================= -->
<div class="container-fluid bg_empresa_home">
    <div class="row">
        <div class="container top5">
            <div class="row">
                <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                <div class="col-12 dicas_titulo empresa_titulo">
                    <div class="col-12 top80">
                        <div class="row">
                            <h4 class="top7 mr-2"> NOSSA</h4>
                            <h2><?php Util::imprime($row1[titulo]); ?></h2>
                        </div>
                    </div>
                    <div class="col-12 p-0">
                        <div class="media">
                            <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png"
                                 alt="">
                            <div class="media-body">
                                <h6 class="mt-0"><?php Util::imprime($row1[legenda]); ?></h6>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-11 top10 ml-auto">
                   <div class="col-6 empresa_des pl-0">
                       <p><?php Util::imprime($row1[descricao], 4000); ?></p>
                   </div>
                    <div class="col-12 p-0 top30">
                        <a class="btn btn-outline-dark btn_atendimento btn_detalhe_empresa" href="<?php echo Util::caminho_projeto() ?>/empresa">
                            MAIS DETALHES
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- EMPRESA    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DICAS -->
<!-- ======================================================================= -->
<div class="container bottom70 top5">
    <div class="row">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
        <div class="col-12 dicas_titulo">
            <div class="col-12">
                <div class="row">
                    <h4 class="top7 mr-2"> <?php Util::imprime($row[legenda_1]); ?></h4>
                    <h2><?php Util::imprime($row[titulo]); ?></h2>
                </div>
            </div>
            <div class="col-12 p-0">
                <div class="media">
                    <img class="top7 mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_verde.png" alt="">
                    <div class="media-body">
                        <h6 class="mt-0"><?php Util::imprime($row[legenda_2]); ?></h6>
                    </div>
                </div>


            </div>

        </div>

        <?php $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
        require_once('./includes/lista_dicas.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
    jQuery(document).ready(function ($) {
        // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
        // it's recommended to disable them when using autoHeight module
        $('#content-slider-1').royalSlider({
            autoHeight: true,
            arrowsNav: true,
            arrowsNavAutoHide: false,
            keyboardNavEnabled: true,
            controlNavigationSpacing: 0,
            controlNavigation: 'tabs',
            autoScaleSlider: false,
            arrowsNavAutohide: true,
            arrowsNavHideOnTouch: true,
            imageScaleMode: 'none',
            globalCaption: true,
            imageAlignCenter: false,
            fadeinLoadedSlide: true,
            loop: false,
            loopRewind: true,
            numImagesToPreload: 6,
            keyboardNavEnabled: true,
            usePreloader: false,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true,
                delay: 9000
            }

        });
    });
</script>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
    <?php /*
    $(window).load(function () {
        $('#slider_carousel').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas
            animationLoop: true,
            itemWidth: 195,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20

        });
    });

    */ ?>

    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 220,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });


</script>

