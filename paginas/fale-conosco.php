<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 0px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top55 bottom65">

        <div class="col-11 ml-auto titulo_internas">
            <h1><?php Util::imprime($banner[titulo]); ?></h1>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO GERAL    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- MENU CONTATOS    -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row barra_fale">
        <div class="col-7">
            <ul class=" nav nav-pills top10 justify-content-center">
                <li class="nav-item nav-block col text-center">
                    <a class=" nav-link active barra_fale_border">
                        FALE CONOSCO
                    </a>
                </li>
                <li class="nav-item col-5 text-center">
                    <a class=" nav-link barra_fale_border"
                       href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                        TRABALHE CONOSCO
                    </a>
                </li>
                <li class="nav-item col text-center">
                    <a class="nav-link" href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
                        ONDE ESTAMOS
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- MENU CONTATOS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- CONTATO -->
<!-- ======================================================================= -->
<?php require_once('./includes/contatos.php') ?>
<!-- ======================================================================= -->
<!-- CONTATO    -->
<!-- ======================================================================= -->


<div class="container-fluid fundo_fale bottom30 top30">
    <div class="row">


        <div class="container ">
            <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
                <div class="row fundo_formulario">


                    <!--  ==============================================================  -->
                    <!-- FORMULARIO CONTATOS-->
                    <!--  ==============================================================  -->
                    <div class=" col-9 ml-auto">


                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="assunto" class="form-control fundo-form"
                                           placeholder="ASSUNTO">
                                    <span class="fas fa-user-circle form-control-feedback"></span>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME">
                                    <span class="fa fa-user form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="email" class="form-control fundo-form"
                                           placeholder="E-MAIL">
                                    <span class="fa fa-envelope form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="tel" name="telefone" class="form-control fundo-form"
                                           placeholder="TELEFONE">
                                    <span class="fa fa-phone form-control-feedback"
                                          data-fa-transform="rotate-90"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="celular" id="celular" class="form-control fundo-form"
                                           placeholder="CELULAR">
                                    <span class="fa fas fa-mobile form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <textarea name="mensagem" cols="15" rows="4" class="form-control fundo-form"
                                              placeholder="MENSAGEM"></textarea>
                                    <span class="fas fa-pencil-alt form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 text-right p-0 ">
                            <button type="submit" class="btn btn-outline-dark btn_atendimento" name="btn_contato">
                                ENVIAR MENSAGEM
                            </button>
                        </div>

                    </div>


                </div>
            </form>
            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->


        </div>
    </div>

</div>
</div>


<!-- ======================================================================= -->
<!-- ONDE ESTAMOS   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/mapa.php') ?>
<!-- ======================================================================= -->
<!-- ONDE ESTAMOS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php

//  VERIFICO SE E PARA ENVIAR O EMAIL
if (isset($_POST[nome])) {
    $texto_mensagem = "
  Assunto: " . ($_POST[assunto]) . " <br />
  Nome: " . ($_POST[nome]) . " <br />
  Email: " . ($_POST[email]) . " <br />
  Telefone: " . ($_POST[telefone]) . " <br />
  Celular: " . ($_POST[celular]) . " <br />


  Mensagem: <br />
  " . (nl2br($_POST[mensagem])) . "
  ";


    if (Util::envia_email($config[email_trabalhe_conosco], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email])) {
        Util::envia_email($config[email_copia], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email]);
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
    } else {
        Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
    }

}


?>


<script>
    $(document).ready(function () {
        $('.FormContatos').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-times',
                validating: 'fas fa-refresh'
            },
            fields: {
                nome: {
                    validators: {
                        notEmpty: {
                            message: 'Insira seu nome.'
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            message: 'Insira sua Mensagem.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Informe um email.'
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor informe seu numero!.'
                        },
                        phone: {
                            country: 'BR',
                            message: 'Informe um telefone válido.'
                        }
                    },
                },
                assunto: {
                    validators: {
                        notEmpty: {}
                    }
                }
            }
        });
    });


</script>
